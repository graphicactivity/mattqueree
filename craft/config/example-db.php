<?php

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
    ),
    'mattqueree.dev' => array(
    	'database' => 'matt',
        'user' => 'root',
        'password' => 'xxx',
    ),
    'mattqueree.co.nz' => array(
    	'database' => 'matt',
        'user' => 'root',
        'password' => 'xxx',
    ),
);
