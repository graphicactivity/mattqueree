<?php

return array(
    '*' => array(
		'siteUrl' => null,
		'defaultWeekStartDay' => 0,
		'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'extraAllowedFileExtensions' => 'json',
        'devMode' => true,
		'cpTrigger' => 'admin',
    ),

    'local' => array(
        'devMode' => true,
        'environmentVariables' => array(
        	'basePath' => '~/Sites/matquerree/public/',
            'baseUrl'  => 'http://matquerree.local',
        )
    ),

    'production' => array(
        'devMode' => false,
        'environmentVariables' => array(
        	'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://mattqueree.com',
        )
    )
);
